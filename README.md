## Projeto para o processo seletivo da Hepta

- Clone o projeto em um diretório de sua preferência
- Edite o arquivo <i>persistence.xml</i>, adicionando a senha para o banco de dados (caso tenha)
- Importe o projeto da IDE de sua preferência
- Realize o deploy em um servidor de aplicação ou um servlet container (Tomcat 9, por exemplo), configurando-o na porta 8080
- Acesse o link http://localhost:8080/mercado_war/ para visualizar o projeto.