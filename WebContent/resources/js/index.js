var inicio = new Vue({
	el:"#inicio",
    data: {

		// Alerta
		alertaTipo: undefined,
		alertaTitulo: undefined,
		alertaTexto: undefined,

		// Indica carregamento da página
		loading: true,

		// Lista de produtos
        listaProdutos: [],

		// Header da lista de produtos
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		],

		// Lista de fabricantes
		listaFabricantes: [],

		// Representa um produto
		produto: {},

		// Controla a edição de registros
		editing: false
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
        vm.buscarFabricantes();
    },
    methods:{

		/**
		 * Apresenta alerta
		 * @param titulo
		 * 		Titulo do alerta
		 * @param texto
		 * 		Texto do alerta
		 */
		mostraAlerta: function(titulo, texto) {
			this.alertaTitulo = titulo;
			this.alertaTexto = texto;
		},

		/**
		 * Apresenta mensagem de sucesso
		 * @param titulo
		 * 		Titulo do alerta
		 * @param texto
		 * 		Texto do alerta
		 */
		mostraAlertaSucesso: function(titulo, texto) {
			console.log('apresentando mensagem de sucesso...');
			this.alertaTipo = 'alert alert-success';
			this.mostraAlerta(titulo, texto);
		},

		/**
		 * Apresenta alerta de erro
		 * @param titulo
		 * 		Titulo do alerta
		 * @param texto
		 * 		Texto do alerta
		 */
		mostraAlertaErro: function(titulo, texto) {
			this.alertaTipo = 'alert alert-danger';
			this.mostraAlerta(titulo, texto);
		},

		/**
		 * Lista todos os produtos cadastrados
		 */
		buscaProdutos: function(){
			const vm = this;
			axios
				.get("http://localhost:8080/mercado_war/rs/produtos/")
				.then(response => {
					vm.listaProdutos = response.data;
				})
				.catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
					vm.error = true;
				}).finally(function() {
					vm.loading = false;
				});
		},

		/**
		 * Remove um produto
		 * @param id
		 * 		Id do produto a ser removido
		 */
		removerProduto: function(id) {
			const vm = this;

			let confirmacao = confirm("Confirma a exclusão do produto?");

			if (confirmacao) {

				axios
					.delete("http://localhost:8080/mercado_war/rs/produtos/" + id)
					.then(response => {
						vm.mostraAlertaSucesso("Sucesso", "Produto removido com sucesso!");
					})
					.catch(function(error) {

					}).finally(function(){
						vm.buscaProdutos();
					});
			}
		},

		/**
		 * Lista todos os fabricantes cadastrados
		 */
		buscarFabricantes: function() {
			axios
				.get("http://localhost:8080/mercado_war/rs/fabricantes")
				.then(response => {
					this.listaFabricantes = response.data;
				})
				.catch(function(error) {
					console.log(error);
				});
		},

		/**
		 * Cadastra produto
		 */
		cadastrarProduto: function() {
			const vm = this;
			axios
				.post("http://localhost:8080/mercado_war/rs/produtos", this.produto)
				.then(response => {
					vm.mostraAlertaSucesso("Sucesso", `Produto "${this.produto.nome}" cadastrado!`);
				})
				.catch(function(error) {
					console.log(error);
					vm.mostraAlertaErro("Erro", `Erro ao cadastrar o produto "${this.produto.nome}"`);
				})
				.finally(function() {
					vm.buscaProdutos();
					vm.limpar();
				});
		},

		/**
		 * Seleciona produto para edição
		 * @param idProduto
		 */
		editarProduto: function(idProduto) {
			const vm = this;
			axios
				.get("http://localhost:8080/mercado_war/rs/produtos/" + idProduto)
				.then(function(response) {
					vm.produto = response.data;
					vm.editing = true;
				})
				.catch(function(error) {
					vm.mostraAlertaErro("Erro", "Erro ao editar produto.");
				});
		},

		/**
		 * Confirma a edição do produto
		 */
		confirmarEdicao: function() {
			const vm = this;
			axios
				.put("http://localhost:8080/mercado_war/rs/produtos/" + vm.produto.id, vm.produto)
				.then(function(response) {
					vm.mostraAlertaSucesso("Sucesso", "Produto alterado com sucesso!");
				})
				.catch(function(error) {
					vm.mostraAlertaErro("Erro", "Não foi possível alterar o produto");
				})
				.finally(function() {
					vm.produto = {};
					vm.buscaProdutos();
					vm.editing = false;
				})
		},

		/**
		 * Limpa formulário
		 */
		limpar: function() {
			this.produto = {}
			this.editing = false;
		}
    }
});