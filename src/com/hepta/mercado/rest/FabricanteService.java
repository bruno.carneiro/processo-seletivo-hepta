package com.hepta.mercado.rest;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.persistence.FabricanteDAO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.List;

@Path("/fabricantes")
public class FabricanteService {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private FabricanteDAO dao;

    public FabricanteService() {
        dao = new FabricanteDAO();
    }

    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Adiciona novo fabricante no mercado
     *
     * @param fabricante: Novo fabricante
     * @return response 200 (OK) - Conseguiu adicionar
     */
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response fabricanteCreate(Fabricante fabricante) {
        try {
            this.dao.save(fabricante);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Erro ao criar fabricante")
                    .build();
        }
        return Response
                .status(Response.Status.OK)
                .entity(fabricante)
                .build();
    }

    /**
     * Lista todos os fabricantes do mercado
     *
     * @return response 200 (OK) - Conseguiu listar
     */
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response fabricantesRead() {
        List<Fabricante> fabricantes = new ArrayList<>();
        try {
            fabricantes = dao.getAll();
        } catch(Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar fabricantes").build();
        }

        GenericEntity<List<Fabricante>> entity = new GenericEntity<List<Fabricante>>(fabricantes) {};
        return Response.status(Response.Status.OK).entity(entity).build();
    }

    /**
     * Procura um fabricante pelo seu ID
     *
     * @param id
     * 		Identificador do fabricante
     * @return response 200 (OK) - Encontrou o fabricante
     */
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response fabricanteReadOne(@PathParam("id") Integer id) {

        Fabricante responseFabricante = null;
        try {
            responseFabricante = this.dao.find(id);
        }
        catch (Exception e) {
            return Response
                    .status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Erro ao buscar o fabricante com o id " + id)
                    .build();
        }

        return Response.status(Response.Status.OK)
                .entity(responseFabricante)
                .build();
    }

    /**
     * Atualiza um fabricante no mercado
     *
     * @param id: id do fabricante
     * @param fabricante: Fabricante atualizado
     * @return response 200 (OK) - Conseguiu atualizar
     */
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response fabricanteUpdate(@PathParam("id") Integer id, Fabricante fabricante) {

        Fabricante fabricanteResponse = null;
        try {
            fabricante.setId(id);
            fabricanteResponse = this.dao.update(fabricante);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Response
                    .status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Erro ao alterar fabricante")
                    .build();
        }
        return Response
                .status(Response.Status.OK)
                .entity(fabricanteResponse)
                .build();
    }

    /**
     * Remove um fabricante do mercado
     *
     * @param id: id do fabricante
     * @return response 200 (OK) - Conseguiu remover
     */
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @DELETE
    public Response fabricanteDelete(@PathParam("id") Integer id) {
        try {
            this.dao.delete(id);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Response
                    .status(Status.INTERNAL_SERVER_ERROR)
                    .entity("Erro ao deletar fabricante")
                    .build();
        }
        return Response
                .status(Response.Status.OK)
                .entity("Fabricante com o id " + id + " removido com sucesso!")
                .build();
    }

}
